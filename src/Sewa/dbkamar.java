/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sewa;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author 
 */
public class dbkamar {
 private String NoKamar;
 private String Jenis;
    private float sewahari;
    
    public String getNoKamar(){return NoKamar;}
    public void setNoKamar(String NoKamar){this.NoKamar=NoKamar;}
    public String getJenis(){return Jenis;}
    public void setJenis(String Jenis){this.Jenis=Jenis;}
    public float getsewahari(){return sewahari;}
    public void setsewahari(float sewahari){this.sewahari=sewahari;}
    
    
public Vector LookUp(String fld,String dt){
try{
Vector tableData=new Vector();
koneksi con= new koneksi();
con.bukaKoneksi();
con.statement=con.dbkoneksi.createStatement();
ResultSet rs= con.statement.executeQuery("Select NoKamar, Jenis, sewahari from kamar where "+fld+" like '%"+dt+"%'");
int i=1;
while (rs.next()){
Vector<Object> row= new Vector<Object>();
row.add(rs.getString("NoKamar"));
row.add(rs.getString("Jenis"));
row.add(rs.getString("sewahari"));

tableData.add(row);
i++;
}
con.tutupkoneksi(); return tableData;
} catch (SQLException ex){
ex.printStackTrace();
return null;
}
}

public boolean insert(){
boolean berhasil=false;
    koneksi con=new koneksi();
    try{
    con.bukaKoneksi();
    con.preparedStatement= con.dbkoneksi.prepareStatement("Insert into kamar"+"(NoKamar,Jenis,sewahari) value (?,?,?)");
        con.preparedStatement.setString(1,this.NoKamar);
        con.preparedStatement.setString(2,this.Jenis);
        con.preparedStatement.setFloat(3,this.sewahari);
       
        con.preparedStatement.executeUpdate();
        berhasil=true;
    } catch(Exception e){
    e.printStackTrace();
    berhasil=false;
    }
    finally{
    con.tutupkoneksi();
    return berhasil;
    }
}
public Vector Load(){
try{
    Vector tableData=new Vector();
    koneksi con=new koneksi();
    con.bukaKoneksi();
    con.statement=con.dbkoneksi.createStatement();
    ResultSet rs= con.statement.executeQuery("Select NoKamar,Jenis,sewahari from kamar");
    int i=1;
    while (rs.next()){
    Vector<Object> row =new Vector<Object>();
    row.add(rs.getString("NoKamar"));
    row.add(rs.getString("Jenis"));
    row.add(rs.getString("sewahari"));
    
    tableData.add(row);
    i++;
    }
    con.tutupkoneksi();
    return tableData;
}catch (SQLException ex){
ex.printStackTrace();
return null;
}
}
public boolean delete(String nomor){
boolean berhasil=false;
koneksi con=new koneksi();
try{
con.bukaKoneksi();
con.preparedStatement=con.dbkoneksi.prepareStatement("delete from kamar where NoKamar"+
 "= ?");
con.preparedStatement.setString(1, nomor);
con.preparedStatement.executeUpdate();
berhasil=true;
}catch(Exception e){
e.printStackTrace();
berhasil=false;
}finally{
con.tutupkoneksi();
return berhasil;}
}
public boolean select(String nomor){
try{
    koneksi con=new koneksi();
    con.bukaKoneksi();
    con.statement=con.dbkoneksi.createStatement();
    ResultSet rs= con.statement.executeQuery("select NoKamar, Jenis, sewahari "+
    "from kamar where NoKamar= '"+nomor+"'");
while(rs.next()){
    this.NoKamar=rs.getString("NoKamar");
    this.Jenis=rs.getString("Jenis");
    this.sewahari=rs.getInt("sewahari");
    
    
}
con.tutupkoneksi();
return true;
} catch(SQLException ex){
ex.printStackTrace();
return false;
}
}
public int validasiNoKamar(String nomor){
int val=0;
try{
 koneksi con = new koneksi();
 con.bukaKoneksi();
 con.statement=con.dbkoneksi.createStatement();
 ResultSet rs= con.statement.executeQuery("select count(*)as jml "+
"from kamar where NoKamar= '"+nomor+"'");
while(rs.next()){
val=rs.getInt("jml");
}
con.tutupkoneksi();
} catch (SQLException ex){
ex.printStackTrace();
}
return val;
}
public boolean update (String nomor){
boolean berhasil=false;
koneksi con= new koneksi();
try{
con.bukaKoneksi();
con.preparedStatement=con.dbkoneksi.prepareStatement("update kamar set Jenis=?"
 +", sewahari =?, botol=?, gelas=? where NoKamar=?");
con.preparedStatement.setString(1, Jenis);
con.preparedStatement.setDouble(2, sewahari);
con.preparedStatement.setString(5, nomor);
con.preparedStatement.executeUpdate();
berhasil=true;
} catch (Exception e){
e.printStackTrace();
berhasil=false;
} finally{
con.tutupkoneksi();
return berhasil;
}
}
}

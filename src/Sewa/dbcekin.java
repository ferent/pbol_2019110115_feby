/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sewa;

import java.sql.Date;
import java.lang.String;
import java.util.*;
import java.sql.*;
import java.text.SimpleDateFormat;


/**
 *
 * @author 2017130036
 */
public class dbcekin {
    private String NoKamar;
    private String NoCekin;
    private Date tanggal;
    private String tamu;
    private String tour;
    private int lama;
    
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd"); 
    
    public String getNoKamar(){return NoKamar;}
    public void setNoKamar(String NoKamar){this.NoKamar=NoKamar;}
    public String getNoCekin(){return NoCekin;}
    public void setNoCekin(String NoCekin){this.NoCekin=NoCekin;}
    public String gettamu(){return tamu;}
    public void settamu(String tamu){this.tamu=tamu;}
    public String gettour(){return tour;}
    public void settour(String tour){this.tour=tour;}
    public Date gettanggal(){return tanggal;}
    public void settanggal(Date tanggal){this.tanggal=tanggal;}
    public int getlama(){return lama;}
    public void setlama(int lama){this.lama=lama;}


public boolean insert(){
boolean berhasil=false;
    koneksi con=new koneksi();
    try{
    con.bukaKoneksi();
    con.preparedStatement= con.dbkoneksi.prepareStatement("Insert into cekin"+
            "(NoCekin,tanggal,tamu,NoKamar,tour,lama) value (?,?,?,?,?,?)");
        con.preparedStatement.setString(1,this.NoCekin);
        con.preparedStatement.setDate(2,this.tanggal);
        con.preparedStatement.setString(3,this.tamu);
        con.preparedStatement.setString(4,this.NoKamar);
        con.preparedStatement.setString(5,this.tour);
        con.preparedStatement.setInt(6,this.lama);
        con.preparedStatement.executeUpdate();
        berhasil=true;
    } catch(Exception e){
    e.printStackTrace();
    berhasil=false;
    }
    finally{
    con.tutupkoneksi();
    return berhasil;
    }
}

public Vector Load(){
try{
    Vector tableData=new Vector();
    koneksi con=new koneksi();
    con.bukaKoneksi();
    con.statement=con.dbkoneksi.createStatement();
    ResultSet rs= con.statement.executeQuery("Select NoCekin, n.NoKamar, "+
         " tanggal,tamu,lama,tour,sewahari from cekin n join kamar m on(n.NoKamar=m.NoKamar) ");
    int i=1;
    while (rs.next()){
    Vector<Object> row =new Vector<Object>();
    row.add(rs.getString("NoCekin"));
    row.add(rs.getDate("Tanggal"));
    row.add(rs.getString("tamu"));
    row.add(rs.getString("NoKamar"));
    row.add(rs.getInt("sewahari"));
    row.add(rs.getString("tour"));
    row.add(rs.getInt("lama"));
    }
    con.tutupkoneksi();
    return tableData;
}catch (SQLException ex){
ex.printStackTrace();
return null;
}
}



public boolean delete(String nomor){
boolean berhasil=false;
koneksi con=new koneksi();
try{
con.bukaKoneksi();
con.preparedStatement=con.dbkoneksi.prepareStatement("delete from cekin where NoCekin"+"= ?");
con.preparedStatement.setString(1, nomor);
con.preparedStatement.executeUpdate();
berhasil=true;
}catch(Exception e){
e.printStackTrace();
berhasil=false;
}finally{
con.tutupkoneksi();
return berhasil;}
}

public boolean select(String nomor){
try{
    koneksi con=new koneksi();
    con.bukaKoneksi();
    con.statement=con.dbkoneksi.createStatement();
    ResultSet rs= con.statement.executeQuery("select NoCekin,NoKamar,tamu,tanggal,lama,tour from cekin where NoCekin='" + nomor+"'");
while(rs.next()){
    this.NoKamar=rs.getString("NoCekin");
    this.NoCekin=rs.getString("NoKamar");
    this.tanggal=rs.getDate("Tanggal");
    this.tamu=rs.getString("tamu");
    this.lama=rs.getInt("lama");
    this.tour=rs.getString("tour");
}
con.tutupkoneksi();
return true;
} catch(SQLException ex){
ex.printStackTrace();
return false;
}
}
public int validasiNoKamar(String nomor){
int val=0;
try{
 koneksi con = new koneksi();
 con.bukaKoneksi();
 con.statement=con.dbkoneksi.createStatement();
 ResultSet rs= con.statement.executeQuery("select count(*)as jml "+
"from cekin where NoCekin= '"+nomor+"'" );
while(rs.next()){
val=rs.getInt("jml");
}
con.tutupkoneksi();
} catch (SQLException ex){
ex.printStackTrace();
}
return val;
}
public boolean update (String nomor){
boolean berhasil=false;
koneksi con= new koneksi();
try{
con.bukaKoneksi();
con.preparedStatement=con.dbkoneksi.prepareStatement("update cekin set tanggal=?,tamu=?,NoKamar=?,lama=?,tour=? where NoCekin=?");
con.preparedStatement.setDate(1, tanggal);
con.preparedStatement.setString(2, tamu);
con.preparedStatement.setString(3, NoKamar);
con.preparedStatement.setInt(4, lama);
con.preparedStatement.setString(5, tour);
con.preparedStatement.setString(6, nomor);

con.preparedStatement.executeUpdate();
berhasil=true;
} catch (Exception e){
e.printStackTrace();
berhasil=false;
} finally{
con.tutupkoneksi();
return berhasil;
}
}
}

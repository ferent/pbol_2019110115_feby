# SQL Manager 2005 Lite for MySQL 3.7.6.2
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : sewa_2019110115


SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `sewa_2019110115`;

CREATE DATABASE `sewa_2019110115`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `sewa_2019110115`;

#
# Structure for the `cekin` table : 
#

DROP TABLE IF EXISTS `cekin`;

CREATE TABLE `cekin` (
  `NoCekIn` char(5) NOT NULL,
  `Tanggal` date DEFAULT NULL,
  `Tamu` varchar(20) DEFAULT NULL,
  `NoKamar` char(3) DEFAULT NULL,
  `Tour` char(1) DEFAULT NULL,
  `Lama` int(3) DEFAULT NULL,
  PRIMARY KEY (`NoCekIn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `kamar` table : 
#

DROP TABLE IF EXISTS `kamar`;

CREATE TABLE `kamar` (
  `NoKamar` char(3) NOT NULL,
  `Jenis` varchar(10) DEFAULT NULL,
  `SewaHari` float(9,0) DEFAULT NULL,
  PRIMARY KEY (`NoKamar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for the `cekin` table  (LIMIT 0,500)
#

INSERT INTO `cekin` (`NoCekIn`, `Tanggal`, `Tamu`, `NoKamar`, `Tour`, `Lama`) VALUES 
  ('00051','2004-11-03','Budi','207','Y',3),
  ('00052','2004-11-03','Andi','211','T',2),
  ('00053','2005-11-03','Daniel','317','Y',7),
  ('00060','2018-12-02','Karin','317','Y',6);

COMMIT;

#
# Data for the `kamar` table  (LIMIT 0,500)
#

INSERT INTO `kamar` (`NoKamar`, `Jenis`, `SewaHari`) VALUES 
  ('207','Single',300000),
  ('211','Double',500000),
  ('256','Single',250000),
  ('317','Family',1000000);

COMMIT;

